from django.contrib import admin

# Register your models here.


from .models import Task


@admin.register(Task)
class TaskAdmin(admin.ModelAdmin):
    list_display = ("name", "start_date", "due_date", "is_complete")
